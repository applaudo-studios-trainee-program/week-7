# Trainee Program - Week 7

## Installation

There is **no necessary configuration**, you just need to use one of the link(s) below to see the last deployed version of the project

## Deployment

You can see the last deployed version of the project by going to any of these links

[Vercel](https://week-7.b-mendoza.vercel.app/)

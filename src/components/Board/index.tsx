import React, { memo } from 'react';

import { BoardState } from 'hooks/useGameState';
import Grid from 'components/Grid';
import Square, { SquareProps } from 'components/Square';

type Props = {
  board: BoardState;
  isUserTraveling: boolean;
  onClick: (square: number) => void;
};

function Board({ board, isUserTraveling, onClick }: Props) {
  const createProps = (square: number): SquareProps => {
    return { value: board[square], onClick: () => onClick(square) };
  };

  return (
    <Grid columns={3} gap={1}>
      {[...new Array<null>(9)].map((_, index) => (
        <Square
          key={index.toString()}
          {...createProps(index)}
          disabled={isUserTraveling}
        />
      ))}
    </Grid>
  );
}

export default memo(Board);

import React, { ReactNode, memo } from 'react';
import styled from '@emotion/styled';

type Props = {
  children: ReactNode;

  type?: 'button' | 'submit';
  disabled?: boolean;

  onClickFn?: () => void;
};

const StyledButton = styled.button`
  background-color: var(--primary-black-color);

  border-radius: var(--border-radius);

  color: var(--primary-white-color);

  font-weight: 500;

  padding: 0.5rem 2rem;

  &:disabled {
    background-color: var(--secondary-black-color);

    color: var(--secondary-white-color);

    pointer-events: none;
  }
`;

function Button({ children, type = 'button', disabled, onClickFn }: Props) {
  return (
    <StyledButton
      type={type === 'button' ? 'button' : 'submit'}
      disabled={disabled}
      onClick={onClickFn}
    >
      {children}
    </StyledButton>
  );
}

export default memo(Button);

import React, { ReactNode, memo } from 'react';
import styled from '@emotion/styled';

type LayoutProps = {
  columns: number;
  gap: number;
};

type Props = {
  columns: number;
  gap: number;
  children: ReactNode;
};

const BREAKPOINTS = [340, 576, 768, 992];

const StyledGrid = styled.div<LayoutProps>`
  height: max-content;

  display: grid;
  gap: ${props => props.gap}rem;
  grid-auto-rows: 3em;
  grid-template-columns: repeat(${props => props.columns}, 1fr);

  ${BREAKPOINTS.map((breakpoint, index) => {
    const baseRowHeight = 2;

    return `@media (min-width: ${breakpoint}px) {
      grid-auto-rows: ${baseRowHeight * (index + 2)}em;
    }`;
  })}
`;

const Grid = ({ columns, gap, children }: Props) => (
  <StyledGrid columns={columns} gap={gap}>
    {children}
  </StyledGrid>
);

export default memo(Grid);

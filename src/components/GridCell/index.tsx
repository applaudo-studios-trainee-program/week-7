import React, { useRef, memo } from 'react';

import { randomRGBColor } from 'utils/randomRGBColor';
import styles from './GridCell.module.css';

type Props = {
  cellId: string;
  disabled: boolean;

  currentCell?: string | null;
  value?: string;

  onClickFn: (cellId: string) => void;
};

function GridCell({ cellId, disabled, currentCell, value, onClickFn }: Props) {
  const rgbColorRef = useRef(randomRGBColor());

  return (
    <button
      style={
        currentCell !== cellId && !disabled
          ? {
              backgroundColor: `rgb(${rgbColorRef.current})`,
              cursor: 'auto'
            }
          : currentCell === cellId && disabled
          ? {
              backgroundColor: `rgb(${rgbColorRef.current})`,
              cursor: 'auto'
            }
          : {
              backgroundColor: `rgba(${rgbColorRef.current}, 0.5)`,
              cursor: 'auto'
            }
      }
      className={styles.item}
      disabled={disabled}
      onClick={() => onClickFn(cellId)}
      type="button"
    >
      {value}
    </button>
  );
}

export default memo(GridCell);

import React, { memo } from 'react';
import styled from '@emotion/styled';

import { Value } from 'hooks/useGameState';

const StyledSquare = styled.button`
  height: auto;
  width: 7em;

  color: var(--primary-white-color);

  background-color: var(--primary-black-color);
  padding: 0;
  font-size: 24px;
  font-weight: bold;

  border: 0.1px solid var(--primary-white-color);
  border-radius: var(--border-radius);
`;

export type SquareProps = {
  value: Value;

  disabled?: boolean;

  onClick: () => void;
};

function Square({ value, disabled, onClick }: SquareProps) {
  return (
    <StyledSquare onClick={onClick} disabled={disabled}>
      {value}
    </StyledSquare>
  );
}

export default memo(Square);

import React from 'react';
import { Link } from 'react-router-dom';

import styles from './Navbar.module.css';

function Navbar() {
  return (
    <nav className={styles.container}>
      <ul className={styles.list}>
        <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="time-machine">Time Machine</Link>
        </li>
        <li>
          <Link to="tic-tac-toe">Tic Tac Toe</Link>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;

import { useCallback, useState } from 'react';

import { calculateWinner } from 'utils/calculateWinner';

export type Value = 'X' | 'O' | null;

export type BoardState = Value[];

export type GameState = { history: BoardState[]; step: number };

const createBoardState = () => Array<Value>(9).fill(null);

export function useGameState() {
  const [gameState, setGameState] = useState<GameState>({
    history: [createBoardState()],
    step: 0
  });

  const current = gameState.history[gameState.step];

  const xIsNext = gameState.step % 2 === 0;

  const winner = calculateWinner(current);

  const handleClick = useCallback(
    (square: number) => {
      const history = gameState.history.slice(0, gameState.step + 1);

      const boardState = history[history.length - 1];

      if (calculateWinner(boardState) || boardState[square]) return;

      const newBoardState = boardState.slice();

      newBoardState[square] = gameState.step % 2 === 0 ? 'X' : 'O';

      history.push(newBoardState);

      setGameState({ history: history, step: history.length - 1 });
    },
    [gameState.history, gameState.step]
  );

  const jumpTo = useCallback(
    (step: number) => {
      setGameState({ history: gameState.history, step });
    },
    [gameState.history]
  );

  const restart = useCallback(() => {
    setGameState({ history: [createBoardState()], step: 0 });
  }, []);

  const replay = useCallback(
    (callback: () => void, delay: number, repetitions: number) => {
      let iterations = 0;

      const intervalID = setInterval(function () {
        callback();

        if (++iterations === repetitions) clearInterval(intervalID);
      }, delay);
    },
    []
  );

  return {
    gameState,
    current,
    xIsNext,
    winner,
    handleClick,
    jumpTo,
    restart,
    replay
  };
}

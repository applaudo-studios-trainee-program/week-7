import { useState, useEffect, useRef, useCallback } from 'react';

export function useTimeMachine<T>(stateToKeepTrack: T) {
  const firstRenderRef = useRef(false);

  const [currentStep, setCurrentStep] = useState(0);
  const [entries, setEntries] = useState<T[]>([stateToKeepTrack]);

  const updateEntries = useCallback(() => {
    setEntries(entries => [stateToKeepTrack, ...entries]);
  }, [stateToKeepTrack]);

  useEffect(() => {
    if (firstRenderRef.current) updateEntries();
  }, [stateToKeepTrack, updateEntries]);

  useEffect(() => {
    firstRenderRef.current = true;
  }, []);

  return [entries, currentStep, setCurrentStep] as const;
}

import Navbar from 'components/UI/Navbar';
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import Home from 'views/Home';

import TicTacToe from 'views/TicTacToe';
import TimeMachine from 'views/TimeMachine';

enum Paths {
  HomePath = '/',
  TimeMachinePath = '/time-machine',
  TicTacToePath = '/tic-tac-toe'
}

function AppRouter() {
  return (
    <Router>
      <Navbar />

      <main>
        <Switch>
          <Route exact path={Paths.HomePath} component={Home} />
          <Route exact path={Paths.TicTacToePath} component={TicTacToe} />
          <Route exact path={Paths.TimeMachinePath} component={TimeMachine} />

          <Redirect to="/" />
        </Switch>
      </main>
    </Router>
  );
}

export default AppRouter;

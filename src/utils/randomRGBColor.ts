export function randomRGBColor() {
  const o = Math.round;
  const r = Math.random;

  const s = 255;

  return `${o(r() * s)}, ${o(r() * s)}, ${o(r() * s)}`;
}

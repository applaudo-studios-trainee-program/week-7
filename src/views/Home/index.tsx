import React from 'react';

import { Link } from 'react-router-dom';
import TMIcon from 'assets/icons/TMIcon';
import TTTIcon from 'assets/icons/TTTIcon';
import styles from './Home.module.css';

function Home() {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Home</h1>

      <div className={styles.target}>
        <Link to="tic-tac-toe" className={styles['anchor-target']}>
          <TTTIcon className={styles['target-icon']} />

          <h2>Tic Tac Toe</h2>
        </Link>
      </div>

      <div className={styles.target}>
        <Link to="time-machine" className={styles['anchor-target']}>
          <TMIcon className={styles['target-icon']} />

          <h2>Time Machine</h2>
        </Link>
      </div>
    </div>
  );
}

export default Home;

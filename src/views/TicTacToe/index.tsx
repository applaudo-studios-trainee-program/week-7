import React, { useCallback, useState } from 'react';

import { Column, Row } from 'components/Layout';
import { useGameState } from 'hooks/useGameState';
import Board from 'components/Board';
import Button from 'components/Button';
import styles from './TicTacToe.module.css';

function TicTacToe() {
  const [isUserReplaying, setIsUserReplaying] = useState(false);

  const {
    gameState,
    current,
    xIsNext,
    jumpTo,
    winner,
    handleClick,
    restart
  } = useGameState();

  const { history, step } = gameState;

  const handleNext = useCallback(() => jumpTo(step - 1), [jumpTo, step]);

  const handlePrevious = useCallback(() => jumpTo(step + 1), [jumpTo, step]);

  const handleResume = useCallback(() => {
    jumpTo(history.length - 1);
  }, [history.length, jumpTo]);

  const handleReplay = useCallback(() => {
    history.forEach((_, index) => {
      setTimeout(() => jumpTo(index), index * 500);

      if (history.length - 1 === index) {
        setTimeout(() => {
          jumpTo(index);

          setIsUserReplaying(false);
        }, index * 500);
      }
    });

    setIsUserReplaying(true);
  }, [history, jumpTo]);

  return (
    <section className={styles.container}>
      <h1 className={styles.title}>Tic Tac Toe Game</h1>

      <Row gap={20}>
        <Board
          board={current}
          isUserTraveling={history.length - 1 !== step}
          onClick={handleClick}
        />

        <section className={styles.actions}>
          <Column gap={20}>
            <Column gap={10}>
              <Button
                onClickFn={handleNext}
                disabled={history.length === 1 || step === 0 || isUserReplaying}
              >
                Next
              </Button>
              <Button
                onClickFn={handlePrevious}
                disabled={history.length - 1 === step || isUserReplaying}
              >
                Previous
              </Button>
              <Button
                onClickFn={handleResume}
                disabled={history.length - 1 === step || isUserReplaying}
              >
                Resume
              </Button>
            </Column>

            <Column gap={10}>
              <span className={styles['game-status']}>
                {history.length - 1 === 9
                  ? 'IS A TIE'
                  : typeof winner !== 'string'
                  ? winner
                    ? `Winner ${winner}`
                    : `Next Player ${xIsNext ? 'X' : 'O'}`
                  : `${winner} has WON!!!`}
              </span>

              <Button
                onClickFn={restart}
                disabled={history.length === 0 || isUserReplaying}
              >
                Restart
              </Button>

              {typeof winner === 'string' ? (
                <Button onClickFn={handleReplay}>Replay</Button>
              ) : null}
            </Column>
          </Column>
        </section>
      </Row>
    </section>
  );
}

export default TicTacToe;

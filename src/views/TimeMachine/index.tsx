import React, { useState, useCallback, useMemo } from 'react';

import { useTimeMachine } from 'hooks/useTimeMachine';
import Button from 'components/Button';
import Grid from 'components/Grid';
import GridCell from 'components/GridCell';
import styles from './TimeMachine.module.css';

type Cell = { cellId: string | null };

function TimeMachine() {
  const [cell, setCell] = useState<Cell>({ cellId: null });

  const [entries, currentStep, setCurrentStep] = useTimeMachine(cell);

  const handleNext = useCallback(
    () => setCurrentStep(currentStep => currentStep + 1),
    [setCurrentStep]
  );

  const handlePrevious = useCallback(
    () => setCurrentStep(currentStep => currentStep - 1),
    [setCurrentStep]
  );

  const handleResume = useCallback(() => setCurrentStep(0), [setCurrentStep]);

  const handleCellClick = useCallback(
    (cellId: string) => setCell({ cellId }),
    []
  );

  const CellList = useMemo(
    () =>
      [...new Array<null>(4 ** 2)].map((_, index) => (
        <GridCell
          key={index.toString()}
          cellId={index.toString()}
          disabled={currentStep !== 0}
          currentCell={entries[currentStep].cellId}
          onClickFn={handleCellClick}
        />
      )),
    [entries, currentStep, handleCellClick]
  );

  return (
    <section className={styles.container}>
      <h1 className={styles.title}>Time Machine</h1>

      <Grid columns={4} gap={2}>
        {CellList}
      </Grid>

      <section className={styles.actions}>
        <Button
          disabled={currentStep === entries.length - 1}
          onClickFn={handleNext}
        >
          Next
        </Button>

        <Button disabled={currentStep === 0} onClickFn={handlePrevious}>
          Previous
        </Button>

        <Button disabled={currentStep === 0} onClickFn={handleResume}>
          Resume
        </Button>
      </section>
    </section>
  );
}

export default TimeMachine;
